<?php

namespace FeedMeNews\Http\Middleware;

use FeedMeNews\User;
//use Illuminate\Http\Request;
use Closure;
use Illuminate\Support\Facades\Request;

class apiAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $token = Request::get('access_token');
//        dd($token);
        $user = null;
        if ($token != null) {
            $user = User::where('token', '=', $token)->first();
        }

//        dd($user);
        if (!$user) {
//        dd('here');
            $info['info'] = 'Token Mismatch!, Please Login with correct token to continue...';
            $info['status'] = 'Authentication Error!';
            $info['status_code'] = '401';

            return responseResult($info);
        }
        return $next($request);
    }
}
