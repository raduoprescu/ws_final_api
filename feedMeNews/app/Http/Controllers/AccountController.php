<?php

namespace FeedMeNews\Http\Controllers;

use Illuminate\Http\Request;
use FeedMeNews\Articles;
use Carbon\Carbon;

class AccountController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
//        dd($request->type);
//        if ($request->type == 'source') {
        if ($request->type == 'sentiment') {
            $data['articles'] = Articles::getSentiment();
            $data['articles'] = array_reverse($data['articles']);
            $data['color_group'] = 'success';
        } else {
//            dd('here');
            $data['articles'] = Articles::getGroupBySource($request->type);
            if ($request->type == 'name') {
                $data['color_group'] = 'info';
            } else {
                $data['color_group'] = 'danger';
            }

        }

        $data['type'] = $request->type;
//        } else if ($request->type == 'location') {
//            $data['articles'] = Articles::getSentiment('location');
//        }
        $count = Articles::count();
        $rand = rand(0, $count);
        $rand_side = rand(0, $count);
//dd($data);
//        $data['articles'] = Articles::take(20)->skip($rand)->get();

//        $data['articles'] = Articles::getSentiment('location');
//        dd($data['articles'][0]);

        $data['side_articles'] = Articles::take(10)->skip($rand_side)->get();
//        $data['cities'] = Articles::distinct('source.name')->get()->toArray();
        $data['cities'] = Articles::distinct('source.name')->get()->toArray();
        $data['keywords'] = Articles::getTrendingKeywords(Carbon::now()->subDays(30)->toIso8601String(), Carbon::now()->toIso8601String());
        $data['keywords'] = array_slice($data['keywords'], 0, 10);
        $data['current_city'] = '';
        $data['current_search'] = '';
        $date = new \DateTime();
        $date->setDate(2017, 12, 1);
        $data['start_date'] = $date->format('m/d/Y');
        $data['current_date'] = date('m/d/Y');
        $data['total_articles'] = $count;


        return view('sources', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
