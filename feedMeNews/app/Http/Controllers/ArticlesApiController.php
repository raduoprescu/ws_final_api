<?php

namespace FeedMeNews\Http\Controllers;

use FeedMeNews\Articles;
use FeedMeNews\Comments;
use FeedMeNews\Reviews;
use Illuminate\Http\Request;

class ArticlesApiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
//        dd($request->all());
        if ($request->location) {
            return Articles::where('source.location', '=', $request->location)->take(10)->get();
        } else if ($request->keyword) {
            return Articles::where('keywords', 'like', '%' . $request->keyword . '%')->take(10)->get();
        } else if ($request->publishDate) {
            return Articles::where('publishDate', '=', $request->publishDate)->take(10)->get();
        } else {
            return Articles::take(10)->get();
        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Articles::find($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    public function comments($id)
    {
//        dd($id . ' comments section');
        $comments = Comments::where('newsArticleId', '=', $id)
            ->get();
        return responseResult($comments);
    }

    public function reviews($id)
    {
//        dd($id . ' comments section');
        $reviews = Reviews::where('newsArticleId', '=', $id)
            ->get();
        return responseResult($reviews);
    }
}
