<?php

namespace FeedMeNews\Http\Controllers;

use Illuminate\Http\Request;
use FeedMeNews\Articles;
use Carbon\Carbon;

class AnalysisController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $request_type = '';
        if ($request->type == 'Sources') {
            $request_type = 'source.name';
        } else if ($request->type == 'Location') {
            $request_type = 'source.location';
        } else if ($request->type == 'Sentiment') {
            $request_type = 'sentiment.value';
        }

        $data['type_group'] = $request->type;

        if ($request->search == null) {
//            $data['sources'] = Articles::distinct('source.name')->get()->toArray();

//            $data['sources'] = array_collapse($data['sources']);
//        dd($articles);

            $articles = Articles::getAnalysis($request_type);
            $sources = [];
            $results = Array();
            foreach ($articles as $rd) {
//            $sources[]=
                $sources[$rd['_id']] = [
                    'source' => $rd['_id'],
                    isset($rd['values'][0]['sentiment']) ? $rd['values'][0]['sentiment'] : '' =>
                        isset($rd['values'][0]['sentiment']) ? $rd['values'][0]['count'] : '',
                    isset($rd['values'][1]['sentiment']) ? $rd['values'][1]['sentiment'] : '' =>
                        isset($rd['values'][1]['sentiment']) ? $rd['values'][1]['count'] : '',
                    isset($rd['values'][2]['sentiment']) ? $rd['values'][2]['sentiment'] : '' =>
                        isset($rd['values'][2]['sentiment']) ? $rd['values'][2]['count'] : '',
                ];
            }
//            dd($sources);
            $sources = array_slice($sources, 0, 12);
            $results = [];
            foreach ($sources as $rd) {
                $negative = isset($rd['positive']) ? $rd['positive'] : 0;
                $positive = isset($rd['negative']) ? $rd['negative'] : 0;
                $neutral = isset($rd['neutral']) ? $rd['neutral'] : 0;
                $sum = $negative + $positive + $neutral;
                $results[] = [isset($rd['source']) ? $rd['source'] : '', isset($rd['positive']) ? $rd['positive'] : 0
                    , isset($rd['negative']) ? $rd['negative'] : 0, isset($rd['neutral']) ? $rd['neutral'] : 0, $sum];
            }
            usort($results, function ($b, $a) {
                return $a['4'] - $b['4'];
            });
            array_unshift($results, ['Source', 'Positive', 'Negative', 'Neutral', '']);
//            dd($results);
            $data['results'] = $results;
            $data['text_search'] = '';
            $date = new \DateTime();
            $date->setDate(2017, 12, 1);
            $data['start_date'] = $date->format('m/d/Y');
            $data['current_date'] = date('m/d/Y');

            $data['keywords'] = Articles::getTrendingKeywords(Carbon::now()->subDays(30)->toIso8601String(), Carbon::now()->toIso8601String());
            $data['keywords'] = array_slice($data['keywords'], 0, 10);
//        dd(array_values($results));
        } else {
//            dd($request->all());
//            dd($request->daterange);

            $date_range = $request->daterange;
            $date_range = explode("-", $date_range);
            $date_part_1 = new Carbon($date_range[0]);
            $date_part_2 = new Carbon($date_range[1]);

            //Need to define combinations here!!!
            $articles = Articles::getAnalysisText($request_type, $request->search, $date_part_1->toIso8601String(), $date_part_2->toIso8601String());
            $sources = [];
            $results = Array();
            foreach ($articles as $rd) {
//            $sources[]=
                $sources[$rd['_id']] = [
                    'source' => $rd['_id'],
                    isset($rd['values'][0]['sentiment']) ? $rd['values'][0]['sentiment'] : '' =>
                        isset($rd['values'][0]['sentiment']) ? $rd['values'][0]['count'] : '',
                    isset($rd['values'][1]['sentiment']) ? $rd['values'][1]['sentiment'] : '' =>
                        isset($rd['values'][1]['sentiment']) ? $rd['values'][1]['count'] : '',
                    isset($rd['values'][2]['sentiment']) ? $rd['values'][2]['sentiment'] : '' =>
                        isset($rd['values'][2]['sentiment']) ? $rd['values'][2]['count'] : '',
                ];
            }
//        dd($sources);
            $sources = array_slice($sources, 0, 12);
            $results = [];
            foreach ($sources as $rd) {
                $negative = isset($rd['positive']) ? $rd['positive'] : 0;
                $positive = isset($rd['negative']) ? $rd['negative'] : 0;
                $neutral = isset($rd['neutral']) ? $rd['neutral'] : 0;
                $sum = $negative + $positive + $neutral;
                $results[] = [isset($rd['source']) ? $rd['source'] : '', isset($rd['positive']) ? $rd['positive'] : 0
                    , isset($rd['negative']) ? $rd['negative'] : 0, isset($rd['neutral']) ? $rd['neutral'] : 0, $sum];
            }
            usort($results, function ($b, $a) {
                return $a['4'] - $b['4'];
            });
            array_unshift($results, ['Source', 'Positive', 'Negative', 'Neutral', '']);
            $data['results'] = $results;
            $data['text_search'] = $request->search;
            $data['start_date'] = $date_range[0];
            $data['current_date'] = $date_range[1];

            $data['keywords'] = Articles::getTrendingKeywords($date_part_1->toIso8601String(), $date_part_2->toIso8601String());
            $data['keywords'] = array_slice($data['keywords'], 0, 10);
        }

//        if ($request->type == 'source.name') {
//            $data['search_group'] = 'source.name';
//        } else if ($request->type == 'source.location') {
//            $data['search_group'] = 'source.location';
//        } else {
//            $data['search_group'] = 'sentiment.value';
//        }

        $data['news'][] = ['Country', 'News Articles'];
        $news = Articles::getGroupByCount('location');
        $final_news = [];
        $final_news[] = ['Country', 'News Articles'];
        foreach ($news as $rd) {
            if ($rd['_id'] == 'UK')
                $rd['_id'] = 'United Kingdom';
//                dd();
            $final_news[] = array_values($rd);
        }
//        dd($final_news);
        $data['final_news'] = $final_news;

        return view('analysis.chart', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

}
