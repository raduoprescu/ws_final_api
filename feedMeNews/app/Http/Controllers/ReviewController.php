<?php

namespace FeedMeNews\Http\Controllers;

use Carbon\Carbon;
use FeedMeNews\Reviews;
use Illuminate\Http\Request;

class ReviewController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->userId && $request->newsId) {
//            dd('reached');
            $user_id = $request->userId;
            $news_id = $request->newsId;
            $review = Reviews::where('userId', '=', $user_id)
                ->where('newsArticleId', '=', $news_id)
                ->get();
        } else if ($request->userId) {
            $user_id = $request->userId;
            $review = Reviews::where('userId', '=', $user_id)
                ->get();
        } else if ($request->newsId) {
            $news_id = $request->newsId;
            $review = Reviews::where('newsArticleId', '=', $news_id)
                ->get();
        } else {
            $review = Reviews::all();
        }

        return responseResult($review);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();

        $data['publishDate'] = date('Y-m-d H:i:s');
        $review = Reviews::create($data);

        return responseResult($review);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
//        dd('here');
        $review = Reviews::find($id);
        return responseResult($review);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();
//        dd($data);
        $review = Reviews::findorFail($id);
//        dd($user);
        $review->update([
            'rating' => $data['rating'],
            'text' => $data['text'],
        ]);
        return responseResult($review);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
//        dd($request->all());
        if ($request->userId && $request->newsId) {
//            dd('user & News: ' . $request->userId . ' - ' . $request->newsId);
            $user_id = $request->userId;
            $news_id = $request->newsId;
            $reviews = Reviews::where('userId', '=', $user_id)
                ->where('newsArticleId', '=', $news_id)
                ->get();
//            dd($reviews);
            foreach ($reviews as $rev) {
                $rev->delete();
            }
        } else if ($request->userId) {
//            dd('user: ' . $request->userId);
            $user_id = $request->userId;
            $reviews = Reviews::where('userId', '=', $user_id)
                ->get();
//            dd($reviews);
            foreach ($reviews as $rev) {
                $rev->delete();
            }
        } else if ($request->newsId) {
//            dd('News: ' . $request->newsId);
            $news_id = $request->newsId;
            $reviews = Reviews::where('newsArticleId', '=', $news_id)
                ->get();
//            dd($reviews);
            foreach ($reviews as $rev) {
                $rev->delete();
            }
        } else {
//            dd('Id: ' . $id);
            Reviews::destroy($id);
        }
        //
        return responseResult('Review Deleted Successfully!');
    }
}
