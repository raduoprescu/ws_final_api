<?php

namespace FeedMeNews\Http\Controllers;

use Carbon\Carbon;
use FeedMeNews\Articles;
use Illuminate\Http\Request;

class ArticlesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
//        dd($request->search);
        $count = Articles::count();
        $data['total_articles'] = $count;
        if ($request->search == null && $request->search_city == null && $request->daterange == null) {

            $rand = rand(0, $count);
            $rand_side = rand(0, $count);
//            $data['articles'] = \FeedMeNews\Articles::orderBy('date', 'DESC')->take(20)->skip($rand)->paginate(8);
//            $data['articles'] = Articles::take(20)->skip($rand)->get();
            $data['articles'] = Articles::orderBy('publishDate', 'DESC')->take(20)->skip($rand)->get();
            $data['side_articles'] = Articles::take(10)->skip($rand_side)->get();
            $data['current_city'] = '';
            $data['current_search'] = '';

            $date = new \DateTime();
            $date->setDate(2017, 12, 1);
            $data['start_date'] = $date->format('m/d/Y');
            $data['current_date'] = date('m/d/Y');
            $data['keywords'] = Articles::getTrendingKeywords(Carbon::now()->subDays(30)->toIso8601String(), Carbon::now()->toIso8601String());
            $data['keywords'] = array_slice($data['keywords'], 0, 10);
//    dd($data);
//            dd($data['articles']->links());
        } else {
//            dd($request->daterange);
            $date_range = $request->daterange;
            $date_range = explode("-", $date_range);
            $date_part_1 = new Carbon($date_range[0]);
            $date_part_2 = new Carbon($date_range[1]);
//            dd($date_part_2->format('Y-m-d H:i:s'));
//dd($date_part_2->toDateString());
            $rand = rand(0, $count);
            $rand_side = rand(0, $count);

            if ($request->search_city != null && $request->search != null) {

                $data['articles'] = Articles::where('source.name', 'like', '%' . $request->search . '%')
                    ->orWhere('keywords', 'like', '%' . $request->search . '%')
                    ->where('source.location', '=', $request->search_city)
                    ->whereBetween('publishDate', [$date_part_1->toDateString(), $date_part_2->toDateString()])
                    ->orderBy('publishDate', 'DESC')->take(20)->get();

            } else if ($request->search_city != null) {

                $data['articles'] = Articles::where('source.location', '=', $request->search_city)
                    ->whereBetween('publishDate', [$date_part_1->toDateString(), $date_part_2->toDateString()])
                    ->orderBy('publishDate', 'DESC')->take(20)->get();

            } else if ($request->search != null) {

                $data['articles'] = Articles::where('source.name', 'like', '%' . $request->search . '%')
                    ->orWhere('keywords', 'like', '%' . $request->search . '%')
                    ->whereBetween('publishDate', [$date_part_1->toDateString(), $date_part_2->toDateString()])
                    ->orderBy('publishDate', 'DESC')->take(20)->get();

            } else if ($request->daterange != null) {

                $data['articles'] = Articles::whereBetween('publishDate', [$date_part_1->toDateString(), $date_part_2->toDateString()])
                    ->orderBy('publishDate', 'DESC')->take(20)->get();

            }

//                ->take(20)->paginate(8);
            $data['side_articles'] = \FeedMeNews\Articles::take(10)->skip($rand_side)->get();
            $data['current_search'] = $request->search;
            $data['current_city'] = $request->search_city;
            $data['start_date'] = $date_range[0];
            $data['current_date'] = $date_range[1];
            $data['keywords'] = Articles::getTrendingKeywords($date_part_1->toIso8601String(), $date_part_2->toIso8601String());
            $data['keywords'] = array_slice($data['keywords'], 0, 10);
        }
//        dd($data['keywords']);
//        dd(Carbon::now()->subDays(15)->toDateString());
        $data['sources'] = Articles::distinct('source.name')->get()->toArray();
        $data['cities'] = Articles::distinct('source.location')->get()->toArray();


        return view('index', $data);//
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
//        $data['articles'] = Articles::where('source.name', 'like', '%' . $request->search . '%')
//            ->orWhere('source.location', '=', $request->search_city)
////                ->orWhereIn('keyword', [$request->search])
//            ->orWhere('keywords', 'like', '%' . $request->search . '%')
//            ->whereBetween('publishDate', [$date_part_1->toDateString(), $date_part_2->toDateString()])
////                ->whereBetween('date', [$date_part_1->format('Y-m-d H:i:s'), $date_part_2->format('Y-m-d H:i:s')])
////                ->where('date', '>=', $date_part_1->format('Y-m-d H:i:s'))
////                ->where('date', '<=', $date_part_2->format('Y-m-d H:i:s'))
//            ->orderBy('publishDate', 'DESC')->get();
////                ->orderBy('publishDate', 'DESC')->get();
////                ->take(20)->paginate(8);
        //                ->orWhere('title', 'like', '%' . $request->search . '%')
//                ->orWhere('description', 'like', '%' . $request->search . '%')
//                ->orWhere('category', 'like', '%' . $request->search . '%')
//                ->where('date', '>=', $date_part_1->toDateString())
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        dd('submitted');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
