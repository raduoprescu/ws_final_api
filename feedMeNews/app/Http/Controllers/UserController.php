<?php

namespace FeedMeNews\Http\Controllers;

use FeedMeNews\Comments;
use FeedMeNews\Reviews;
use Illuminate\Http\Request;
use FeedMeNews\User;
use Illuminate\Support\Facades\Hash;


class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->name) {
            $name = explode(' ', $request->name);
//            dd($name);
            $user = User::where('firstName', '=', $name[0])
                ->where('lastName', '=', $name[1])
                ->first();
        } else {
            $user = User::all();
        }

        return responseResult($user);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $data['encodedPassword'] = Hash::make($data['password']);
        unset($data['password']);
//        dd($data);
        $user = User::create($data);

        return responseResult($user);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::find($id);

        return responseResult($user);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
//        dd('here');
        $data = $request->all();

        $user = User::findorFail($id);
//        dd($user);
        $user->update([
            'firstName' => $data['firstName'],
            'lastName' => $data['lastName'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
        ]);
        return responseResult($user);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        User::destroy($id);
        return responseResult('User Deleted Successfully!');
    }

    public function comments($id)
    {
//        dd($id . ' comments section');
        $comments = Comments::where('userId', '=', $id)
            ->get();
        return responseResult($comments);
    }

    public function reviews($id)
    {
//        dd($id . ' comments section');
        $reviews = Reviews::where('userId', '=', $id)
            ->get();
        return responseResult($reviews);
    }
}
