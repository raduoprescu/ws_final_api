<?php
/**
 * Created by PhpStorm.
 * User: hyds
 * Date: 12/8/2017
 * Time: 5:45 PM
 */

function responseResult($data)
{
    //data array to be sent
//    $currentRoute = Route::current();
//    dd($currentRoute);
//    $locator = explode(".", $currentRoute->getName());
    $code = 200;
    $msg = 'Ok';
    $data = [
        'Response' => [
            'Uri' => Request::path(),
//            'UriDescription' => config('yombo.Uri')[$currentRoute->getName()]['UriDescription'],
//            'DocUri' => config('yombo.Uri')[$currentRoute->getName()]['DocUri'],
//            'Locator' => ucfirst($locator[2]),
            'LocatorType' => 'Object',
//            'User_Info' => $user_info,
//            $type => $typeData,
            'data' => $data,
        ],
        'Code' => $code,
        'Message' => $msg,
//        'HTMLMessage' => config('yombo.Uri')[$currentRoute->getName()]['HTMLMessage'],
    ];
    return $data;
}


function getRandomString($length = 8) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $string = '';

    for ($i = 0; $i < $length; $i++) {
        $string .= $characters[mt_rand(0, strlen($characters) - 1)];
    }

    return $string;
}