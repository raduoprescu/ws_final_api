<?php

namespace FeedMeNews;


use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use DB;

class MongoQueries extends Eloquent
{
    protected $connection = 'mongodb';
    protected $collection = 'google_places';
    protected $guarded = [];

    //Mongodb Queries
    public static function Aggregation() {
    	//Monogdb Aggregation Query

    }

    public static function Ranking() {
    	//Monogdb Ranking Query

    }

    public static function Selection() {
    	//Monogdb Selection Query

    	//db.restaurants.find()

    }

    public static function Projection() {
    	//Monogdb Projection Query

    }

}
