<?php

namespace FeedMeNews;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use DB;

class Reviews extends Eloquent
{
    protected $connection = 'mongodb';
    protected $collection = 'reviews';
    protected $guarded = [];
}
