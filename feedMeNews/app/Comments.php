<?php

namespace FeedMeNews;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use DB;

class Comments extends Eloquent
{
    protected $connection = 'mongodb';
    protected $collection = 'comments';
    protected $guarded = [];
}
