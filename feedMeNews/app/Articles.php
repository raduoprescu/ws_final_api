<?php

namespace FeedMeNews;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use DB;

class Articles extends Eloquent
{
    protected $connection = 'mongodb';
    protected $collection = 'articles';
    protected $guarded = [];

    public static function getGroupBySource($name)
    {
        $queryBuilder = DB::connection('mongodb')->collection('articles');
        $results = $queryBuilder->raw(function ($collection) use ($name) {
            $test_result = $collection->aggregate(
                [
                    ['$sort' => ['publishDate' => -1]],
//                    ['$limit' => 10],
                    ['$group' => ['_id' => '$source.' . $name, 'news' => ['$push' => '$$ROOT']]],
                ]
            );
//dd('wer are ');
            $array = iterator_to_array($test_result);
            $json = \MongoDB\BSON\toJSON(\MongoDB\BSON\fromPHP($array));
            return json_decode($json, true);
        });
//dd($results);
        return $results;
    }

    public static function getSentiment()
    {
        $queryBuilder = DB::connection('mongodb')->collection('articles');
        $results = $queryBuilder->raw(function ($collection) {
            $test_result = $collection->aggregate(
                [
                    ['$sort' => ['publishDate' => -1]],
//                    ['$limit' => 10],
                    ['$group' => ['_id' => '$sentiment.value', 'news' => ['$push' => '$$ROOT']]],
                ]
            );
            $array = iterator_to_array($test_result);
            $json = \MongoDB\BSON\toJSON(\MongoDB\BSON\fromPHP($array));
            return json_decode($json, true);
        });
//dd($results);
        return $results;
    }

    public static function getAnalysis($type)
    {
        //Count positive, negative, neutral
        $queryBuilder = DB::connection('mongodb')->collection('articles');
        $results = $queryBuilder->raw(function ($collection) use ($type) {
            $test_result = $collection->aggregate(
                [
//                    ['$match' => ['publishDate' => ['$gte' => $start_date, '$lt' => $end_date]]],
                    ['$group' => ['_id' => ['name' => '$' . $type, 'sentiment' => '$sentiment.value'], 'count' => ['$sum' => 1]]],
                    ['$sort' => ['count' => 1]],
                    ['$group' => ['_id' => '$_id.name', 'values' => ['$push' => ['sentiment' => '$_id.sentiment', 'count' => '$count']]]],

                ]
            );
            $array = iterator_to_array($test_result);
            $json = \MongoDB\BSON\toJSON(\MongoDB\BSON\fromPHP($array));
            return json_decode($json, true);
        });
//dd($results);
        return $results;
    }

    public static function getAnalysisText($type, $text, $start_date, $end_date)
    {
        //Count positive, negative, neutral
        $queryBuilder = DB::connection('mongodb')->collection('articles');
        $results = $queryBuilder->raw(function ($collection) use ($type, $text, $start_date, $end_date) {
            $test_result = $collection->aggregate(
                [
//                    ['$project' => ['' => -1]],
                    ['$match' => ['$text' => ['$search' => $text]]],
                    ['$match' => ['publishDate' => ['$gte' => $start_date, '$lt' => $end_date]]],
                    ['$group' => ['_id' => ['name' => '$' . $type, 'sentiment' => '$sentiment.value'], 'count' => ['$sum' => 1]]],
                    ['$sort' => ['count' => 1]],
                    ['$group' => ['_id' => '$_id.name', 'values' => ['$push' => ['sentiment' => '$_id.sentiment', 'count' => '$count']]]],

                ]
            );
            $array = iterator_to_array($test_result);
            $json = \MongoDB\BSON\toJSON(\MongoDB\BSON\fromPHP($array));
            return json_decode($json, true);
        });
//dd($results);
        return $results;
    }


    public static function getGroupByCount($name)
    {
        $queryBuilder = DB::connection('mongodb')->collection('articles');
        $results = $queryBuilder->raw(function ($collection) use ($name) {
            $test_result = $collection->aggregate(
                [
                    ['$sort' => ['publishDate' => -1]],
//                    ['$limit' => 10],
                    ['$group' => ['_id' => '$source.' . $name, 'count' => ['$sum' => 1]]],
//                    ['$group' => ['_id' => ['name' => '$' . $type, 'sentiment' => '$sentiment.value'], 'count' => ['$sum' => 1]]],

                ]
            );
//dd('wer are ');
            $array = iterator_to_array($test_result);
            $json = \MongoDB\BSON\toJSON(\MongoDB\BSON\fromPHP($array));
            return json_decode($json, true);
        });
//dd($results);
        return $results;
    }

    public static function getTrendingKeywords($start_date, $end_date)
    {
        $queryBuilder = DB::connection('mongodb')->collection('articles');
        $results = $queryBuilder->raw(function ($collection) use ($start_date, $end_date) {
            $test_result = $collection->aggregate(
                [

//                    ['$project' => ['keywords' => ['$split' => ['$keywords', " "]], 'publishDate' => 1]],
//                    ['$unwind' => '$keywords'],
//                    ['$sort' => ['publishDate' => -1]],
//                    ['$unwind' => '$keywords'],
                    ['$unwind' => '$keywords'],
                    ['$match' => ['publishDate' => ['$gte' => $start_date, '$lt' => $end_date]]],
//                    ['$limit' => 10],
                    ['$group' => ['_id' => '$keywords', 'count' => ['$sum' => 1]]],
                    ['$sort' => ['count' => -1]],
//                    ['$group' => ['_id' => ['name' => '$' . $type, 'sentiment' => '$sentiment.value'], 'count' => ['$sum' => 1]]],
//                    ['$split' => ['$keywords', ', ']],
                ]
            );
//dd('wer are ');
            $array = iterator_to_array($test_result);
            $json = \MongoDB\BSON\toJSON(\MongoDB\BSON\fromPHP($array));
            return json_decode($json, true);
        });
//dd($results);
        return $results;
    }
}



