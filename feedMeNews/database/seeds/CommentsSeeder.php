<?php

use Illuminate\Database\Seeder;
use FeedMeNews\User;
use FeedMeNews\Articles;
use FeedMeNews\Comments;

class CommentsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
//        foreach (Comments::get() as $review) {
//            $review->delete();
//        }

//        dd(Comments::count());
        $news = Articles::take(100)->get();
//        dd($news->random());
        $users = User::get();
//        dd($news);
        $faker = Faker\Factory::create();

        foreach ($users as $user) {
            for ($i = 0; $i < 10; $i++) {
                Comments::create([
                    'userId' => $user->_id,
                    'newsArticleId' => $news->random()->_id,
                    'text' => $faker->realText(rand(50, 100)),
                    'publishDate' => $faker->date('Y-m-d H:i:s'),
                ]);
            }
        }
//        dd($news);
    }
}
