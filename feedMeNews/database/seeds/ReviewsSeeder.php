<?php

use Illuminate\Database\Seeder;
use FeedMeNews\Articles;
use FeedMeNews\Reviews;
use FeedMeNews\User;

class ReviewsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
//        foreach (Reviews::get() as $review) {
//            $review->delete();
//        }
//
        dd(Reviews::count());
        $news = Articles::take(100)->get();
//        dd($news->random());
        $users = User::get();
//        dd($news);
        $faker = Faker\Factory::create();

        foreach ($users as $user) {
            for ($i = 0; $i < 10; $i++) {
                Reviews::create([
                    'userId' => $user->_id,
                    'newsArticleId' => $news->random()->_id,
                    'rating' => $faker->randomElement([1, 2, 3, 4, 5]),
                    'text' => $faker->realText(rand(50, 100)),
                    'publishDate' => $faker->date('Y-m-d H:i:s'),
                ]);
            }
        }
    }
}
