<div class="row">
    <div class="col-md-9">
        {!! Form::open(['url' => '/', 'method' => 'get','id'=>'search_form']) !!}


        <div class="input-group">
            <input id="search_input" name="search" type="text" class="form-control" placeholder="#Keywords"

                   aria-label="Search for..." value="{{$current_search ? $current_search:''}}">
            <input id="search_city" name="search_city" type="hidden" value="{{$current_city ? $current_city:''}}">
            <span class="input-group-btn">
        <button class="btn btn-info" type="submit">Search!</button>
                </span>
        </div>

    </div>
    <div class="col-md-3">
        <div class="dropdown">
            <button class="btn btn-info dropdown-toggle" type="button" id="location_dropdown"
                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                {{$current_city ? $current_city:'Source Location'}}
            </button>
            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                @foreach($cities as $city)
                    <a class="dropdown-item search_city" href="#">{{$city[0]}}</a>
                @endforeach
            </div>
        </div>
    </div>

</div>
<br>
<div class="col-12">
    <div class="row">
        <div class="col-8" style="margin-left: -15px;">
            Date Range: <input type="text" name="daterange" class="form-control"
                               value="{{$start_date}} - {{$current_date}}"/>
        </div>
        <div class="col-4" style="margin-top: 21px;">
            {{--<div class="row">--}}
            <div class="dropdown">
                <button class="btn btn-info dropdown-toggle" type="button" id="group_dropdown"
                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Group
                </button>
                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">

                    <a class="dropdown-item search_city" href="{{url('account?type=name')}}">Sources</a>
                    <a class="dropdown-item search_city" href="{{url('account?type=location')}}">Location</a>
                    <a class="dropdown-item search_city" href="{{url('account?type=sentiment')}}">Sentiment</a>

                </div>
            </div>
            {{--</div>--}}
        </div>
    </div>

</div>


<br>
@foreach($keywords as $key)
    <a style="margin-right: 2px;margin-bottom: 10px;" type="button" href="#"
       class="btn btn-sm btn-outline-info keywords">#{{$key['_id']}}</a>&nbsp;&nbsp;&nbsp;&nbsp;
@endforeach

{{--<a type="button" href="#" class="btn btn-outline-info keywords">#Technology</a>&nbsp;&nbsp;&nbsp;&nbsp;--}}
{{--<a type="button" href="#" class="btn btn-outline-info keywords">#Science</a>&nbsp;&nbsp;&nbsp;&nbsp;--}}
{{--<a type="button" href="#" class="btn btn-outline-info keywords">#Politics</a>&nbsp;&nbsp;&nbsp;&nbsp;--}}
{{--<a type="button" href="#" class="btn btn-outline-info keywords">#Entertainment</a>&nbsp;&nbsp;&nbsp;&nbsp;--}}
{{--<a type="button" href="#" class="btn btn-outline-info keywords">#Travel</a>&nbsp;&nbsp;&nbsp;&nbsp;--}}
{!! Form::close() !!}
<br>