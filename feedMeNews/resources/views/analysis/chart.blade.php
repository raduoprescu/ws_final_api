<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>FeedMeNews Analytics</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css"
          integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">


    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
            integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
            crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js"
            integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh"
            crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js"
            integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ"
            crossorigin="anonymous"></script>

    <script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>

    <!-- Include Date Range Picker -->
    <script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css"/>

    <!--Load the AJAX API-->
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

</head>
<body>
<div class="container">
    <!-- Content here -->
    <div class="row">
        <div class="col-md-12" style="margin-top: 10px;">
            <a href="{{url('/analysis?type=Sources')}}"><img src="{{asset('images/logo.png')}}"></a>
            @if (Route::has('login'))
                <div style="float: right;">

                    <a href="{{ url('/register') }}" class="text-info">Account & Settings</a>

                </div>
            @endif
        </div>
    </div>

    <div class="row" style="margin-top: 10px">
        <div class="col-6">
            {!! Form::open(['url' => '/analysis', 'method' => 'get','id'=>'search_form']) !!}


            <div class="input-group">
                <input id="search_input" value="{{$text_search}}" name="search" type="text"
                       class="form-control"
                       placeholder="#Keywords"

                       aria-label="Search for...">
                <input id="search_group" name="type" type="hidden" value="{{$type_group}}">
                <span class="input-group-btn">
        <button class="btn btn-info" type="submit">Search!</button>
                </span>
            </div>

        </div>
        <div class="col-4">
            <input type="text" name="daterange" class="form-control"
                   value="{{$start_date}} - {{$current_date}}"/>

        </div>
        <div class="col-2">
            <div class="dropdown">
                <button class="btn btn-info dropdown-toggle" type="button" id="group_dropdown"
                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    {{$type_group}}
                </button>
                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">

                    <a class="dropdown-item type_group" href="#">Sources</a>
                    <a class="dropdown-item type_group" href="#">Location</a>
                    <a class="dropdown-item type_group" href="#">Sentiment</a>

                </div>
            </div>
        </div>

    </div>
    <br>
    @foreach($keywords as $key)
        <a style="margin-right: 2px;margin-bottom: 10px;" type="button" href="#"
           class="btn btn-sm btn-outline-info keywords">#{{$key['_id']}}</a>&nbsp;&nbsp;&nbsp;&nbsp;
    @endforeach


    {!! Form::close() !!}

    <div class="row">
        <div class="col-8">
            <hr>
            <h3>Analysis Results</h3>
            <hr>
            <div id="chart_div"></div>
            <br>
            <div id="regions_div" style="width: 900px; height: 500px;"></div>
        </div>
        <div class="col-4">
            {{--@include('sidebar')--}}

        </div>
    </div>
    <div class="row">
        <div class="col-8">


        </div>

    </div>

</div>


</div>
<script type="text/javascript">
    $(function () {
        $('input[name="daterange"]').daterangepicker();
    });

    $('.type_group').click(function () {
        $('#group_dropdown').text($(this).text());
        $('#search_group').val($(this).text());
        $('#search_form')[0].submit();
    });

    $(".keywords").click(function () {
//        alert($(this).text().substring(1));
        $('#search_input').val($(this).text().substring(1));
        $('#search_form')[0].submit();
    });
    $('.pagination li').addClass('page-item');
    $('.pagination li a').addClass('page-link');
    $('.pagination li span').addClass('page-link');
</script>
<script type="text/javascript">

    var chart_data = {!! json_encode($results) !!};
    var map_data = {!! json_encode($final_news) !!};
    //    console.log(chart_data);
    google.charts.load('current', {packages: ['corechart', 'bar']});
    google.charts.setOnLoadCallback(drawStacked);

    function drawStacked() {
        var data = google.visualization.arrayToDataTable(chart_data);
        var view = new google.visualization.DataView(data);
        view.setColumns([0, 1, 2, 3,
            {
                calc: "stringify",
                sourceColumn: 4,
                type: "string",
                role: "annotation"
            }]);
        var options = {
            title: 'Articles based on sources categorized by sentiment',
//            chartArea: {width: '60%'},
            width: 1200,
            height: 500,
            bar: {groupWidth: '50%'},
            legend: {position: 'top', maxLines: 3},
            isStacked: true,
            hAxis: {
                title: 'Number of Articles',
                minValue: 0,
            },
            vAxis: {
                title: 'Sources'
            },
            annotations: {
                textStyle: {
                    fontSize: 14
                },
//                color: "black",
            },
        };
        var chart = new google.visualization.BarChart(document.getElementById('chart_div'));
        chart.draw(view, options);
    }


    google.charts.load('current', {
        'packages': ['geochart'],
        // Note: you will need to get a mapsApiKey for your project.
        // See: https://developers.google.com/chart/interactive/docs/basic_load_libs#load-settings
        'mapsApiKey': 'AIzaSyD-9tSrke72PouQMnMX-a7eZSW0jkFMBWY'
    });
    google.charts.setOnLoadCallback(drawRegionsMap);

    function drawRegionsMap() {
        var data = google.visualization.arrayToDataTable(map_data);

        var options = {};

        var chart = new google.visualization.GeoChart(document.getElementById('regions_div'));

        chart.draw(data, options);
    }
</script>
</body>
</html>
