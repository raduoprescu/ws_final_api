<div class="card border-secondary">
    <div class="card-header bg-info text-white">News you may like</div>
    <div class="card-body">
        @foreach($side_articles as $article)

            <a target="_blank" href="{{$article->url}}"><img width="80" height="50"
                                                             src="{{config('news.'.$article->source['name'])}}">
                <h4 style="display: inline;"
                    class="card-title text-info">{{$article->source['name']}}</h4></a>
            <a target="_blank" href="{{$article->url}}"><p
                        class="card-title text-danger">{{$article->title}}</p></a>
            <span style="color: red;">{{$article->source['location']}}</span> | <span
                    style="color: green;">{{ Carbon\Carbon::parse($article->publishDate)->diffForHumans()}}</span>
            <br><br>
            <p class="card-text">{{str_limit($article->description, 120)}}</p>
            <hr>
        @endforeach
    </div>
</div>