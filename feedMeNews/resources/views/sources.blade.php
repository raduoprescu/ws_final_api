<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>FeedMeNews</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css"
          integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">


    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
            integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
            crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js"
            integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh"
            crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js"
            integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ"
            crossorigin="anonymous"></script>
</head>
<body>
<div class="container">
    <!-- Content here -->
    <div class="row">
        <div class="col-md-12" style="margin-top: 50px;">
            <img src="{{asset('images/logo.png')}}">
            @if (Route::has('login'))
                <div style="float: right;">

                    <a href="{{ url('/register') }}" class="text-info">Account & Settings</a>

                </div>
            @endif
        </div>
    </div>
    <br><br>
    <div class="row">
        <div class="col-8">
            @include('input')

            <br>
            <h3>Results (Top News by {{ucfirst($type)}}) - {{$total_articles}} Articles</h3>
            <hr>
            @foreach($articles as $article)
                @if($article['_id']=='positive')
                    {{--*/ $var = 'test' /*--}}
                    <?php $color_group = 'success'; ?>
                @elseif($article['_id']=='negative')
                    <?php $color_group = 'danger'; ?>
                @elseif($article['_id']=='neutral')
                    <?php $color_group = 'info'; ?>
                @endif
                <div class="card bg-light border-{{$color_group}}">
                    <div class="card-header bg-{{$color_group}} text-white">
                        {{ucfirst($article['_id'])}}
                        - {{count($article['news'])}}
                        Articles
                    </div>
                    <div class="card-body text-success">
                        @foreach(array_slice($article['news'], 0, 2) as $record)
                            <div class="row">
                                <div class="col-2">
                                    <img width="110" height="75"
                                         src="{{config('news.'.$record['source']['name'])}}">
                                </div>
                                <div class="col-8">

                                    <a href="{{$record['url']}}" target="_blank"><h4
                                                class="card-title text-info">{{$record['source']['name']}}</h4></a>
                                    <span style="color: red;">{{$record['source']['location']}}</span> | <span
                                            style="color: green;">{{ Carbon\Carbon::parse($record['publishDate'])->diffForHumans()}}</span>
                                    @if($type=='sentiment')
                                        |
                                        <span class="badge badge-secondary">Confidence: {{$record['sentiment']['confidence'] }}</span>
                                    @endif

                                    <a target="_blank" href="{{$record['url']}}"><p
                                                class="card-title text-danger">{{$record['title']}}</p></a>
                                    <p class="card-text">{{str_limit($record['description'], 200)}}</p>

                                    @foreach(array_slice($record['keywords'], 0, 5) as $word)
                                        <a href="#" class="badge badge-info">#{{$word}}</a>&nbsp;&nbsp;
                                        &nbsp;&nbsp;
                                    @endforeach

                                </div>
                            </div>
                            <hr>
                        @endforeach
                        <a style="float: right;" href="#"><span>More News...</span></a>
                    </div>
                </div>
                <br>
            @endforeach


        </div>
        <div class="col-4">
            @include('sidebar')

        </div>
    </div>
    <div class="row">
        <div class="col-8">


        </div>

    </div>

</div>


</div>
</body>
</html>
